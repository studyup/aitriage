@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('messages.site.login') }}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/login">
                    	<input type="hidden" name="locale" value="{{ App::getLocale() }}"/>
                    	<input type="hidden" name="partner" value="envive"/>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('login') || session()->has('doctor.inactive') ? ' has-error' : '' }}">
                            <label for="login" class="col-md-5 control-label">{{ trans('messages.site.login_email_or_username') }}</label>

                            <div class="col-md-5">
                                <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}" required autofocus autocomplete="off">

                                @if ($errors->has('login'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                @endif
                                @if(session()->has('doctor.inactive'))
                                    <span class="help-block">
                                        <strong>{{trans('messages.doctor.inactive')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-5 control-label">{{ trans('messages.site.password') }}</label>
                            <span id="capitalTip" style="display:none; color:red">{{ trans('messages.site.CapsLockOn') }}</span>

                            <div class="col-md-5">
                                <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
<!--                                         <input type="checkbox" name="remember"> Remember Me -->
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('messages.site.login') }}
                                </button>
<!--
                                <a class="btn btn-link" href="/password/reset/{{ App::getLocale() }}">
                                    {{ trans('messages.site.forgot_your_password') }}
                                </a>
-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('foot')
<script src='/js/jquery.capslockstate.js'></script>
<script type="text/javascript">
$(function(){
    $(window).bind("capsOn", function(event) {
        if ($("#password:focus").length > 0) {
            $("#capitalTip").show();
        }
    });
    $(window).bind("capsOff capsUnknown", function(event) {
        $("#capitalTip").hide();
    });
    $("#password").bind("focusout", function(event) {
        $("#capitalTip").hide();
    });
    $("#password").bind("focusin", function(event) {
        if ($(window).capslockstate("state") === true) {
            $("#capitalTip").show();
        }
    });

    /* 
    * Initialize the capslockstate plugin.
    * Monitoring is happening at the window level.
    */
    $(window).capslockstate();
})
</script>
@endsection
