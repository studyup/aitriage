<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE = edge, chrome = 1">
	<title>403</title>
	<link href = "https://fonts.googleapis.com/css?family=Lato:100" rel = "stylesheet" type = "text/css">
	<link rel="stylesheet" type="text/css" href="/css/errors.css"/>
</head>
<body>
	<div class = "container">
		<div class = "content">
			<div class = "title">403 Forbidden</div>
			<a href="/" class="goback">返回主页</a>
 		</div>
	</div>
</body>
</html>