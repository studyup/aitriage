<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>智能问诊</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
	<meta content="" name="description">
    
    <link href="/build/css/mint-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ mix('/css/pt-chat.css', 'build') }}" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        window.Laravel = <?php 
            echo json_encode([
                'csrfToken' => csrf_token(),
                'locale' => App::getLocale()
            ]); ?>;
        window.Envive = {
            partner: '{{ $partner }}',
        }

        window.DiagLanguages = {
            localizedStrings: <?php echo json_encode(Lang::get('patient')); ?>,
        }

        function runCmd(cmd) {
            var has = false;
            if (cmd.length > 0) {
                var cs = $('label').filter(function() {
                    return $(this).text().trim() == cmd;
                });
                if (cs.length < 1) {
                    cs = $('label').filter(function() {
                        return $(this).text().trim().indexOf(cmd) != -1;
                    });
                }
                if (cs.length > 0) {
                    has = true;
                    cs[0].click();
                }
                else if ($("button:contains('" + cmd + "')" ).length > 0) {
                    has = true;
                    $("button:contains('" + cmd + "')" ).click()  
                } else if ($("td:contains('" + cmd + "')").length > 0) {
                    $("td:contains('" + cmd + "')").parent().click();
                }
            }
            return has;
        }

        function stopRecord() {
            api.sendEvent({
                name: 'stopRecord'
            });
        }
        function startRecord() {
            api.sendEvent({
                name: 'startRecord'
            })
        }
        function command(cmd) {
            if (cmd.trim().length ==0) 
                return;
            if (cmd.indexOf('确认') != -1) {
                runCmd('确认')
                startRecord();
            } else {
                // window.parent.test('abc')
                if ($('textarea').length > 0) {
                    $("textarea").val($("textarea").val() + cmd).get(0).dispatchEvent(new Event('input'))
                    startRecord();
                } else {
                    var has = false;
                    for(var i = cmd.length; i > 0; i--) {
                        has = runCmd(cmd.substr(0, i))
                        if (has) {
                            break;
                        }
                    }
                    if (has == false) {
                        for(var i = 0 ; i < cmd.length; i++) {
                            has = runCmd(cmd.substr(i))
                            if (has) {
                                break;
                            }
                        }
                    }
                    startRecord();
                }
            }
        }
    </script>
    <style>
        [v-cloak] {
            display: none;
        }
        .mint-button-text {
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div id="pt-chat">
        <keep-alive>
            <router-view></router-view>
        </keep-alive>
    </div><!-- #pt-chat -->
    
	@section('foot')
        <script src="{{ mix('/js/manifest.js', 'build') }}" type="text/javascript"></script>
        <script src="{{ mix('/js/vendor.js', 'build') }}" type="text/javascript"></script>
		<script src="{{ mix('/js/pt-chat.js', 'build') }}" type="text/javascript"></script>
        <script type="text/javascript">
            apiready = function() {
                // api.sendEvent({
                //     name: 'startRecord',
                //     extra: 'cccccc'
                // });
            }

        </script>
	@show
</body>
</html>