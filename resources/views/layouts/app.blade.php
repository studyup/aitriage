<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
	@section('head')
		@include('widgets.head')
	@show
</head>
<body class='overflow-hidden account-body'>
    <div id="app" class="tight account-tight">
    	@section('header')
    		@include('widgets.header')
    	@show

        @yield('content')
    </div>

	@section('footer')
		@include('widgets.footer')
	@show
	
	@section('foot')
        <!-- Scripts -->
        <!--  script src="/js/app.js" type="text/javascript"></script> // conflict with bootstrap -->
    	<!--  script src="/js/main.js" type="text/javascript"></script -->
	@show
	{!! $currentData['script'] or '' !!}

	@include('widgets.external-footer')
</body>
</html>