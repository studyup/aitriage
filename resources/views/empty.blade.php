@extends('layouts.app')

@section('head')
	@parent
<style>
</style>
@endsection

@section('header')
<nav class="navbar navbar-default navbar-static-top">
    <div class="container-full">
    	<span>left</span>
    	<span class="pull-right">right</span>
	</div>
</nav>
@endsection

@section('content')
@endsection

@section('footer')
<div class=" navbar-fixed-bottom">
    <div class="panel-group" id="dx-group" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
			<div class="panel-heading" id="dx-heading" data-toggle="collapse" data-parent="#dx-group" href="#dx-collapse" aria-expanded="true" aria-controls="dx-collapse">
				<div class="container-full">
					<span class="panel-title">Dx</span>
					<span id="dx-sum">dx sum</span>
				</div>
			</div>
			<div class="panel-collapse collapse dx-scroll-area" id="dx-collapse" role="tabpanel" aria-labelledby="dx-heading">
				<div class="panel-body">
					<div class="container-full">
						<div class="row" id="dx-full">dx full</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection