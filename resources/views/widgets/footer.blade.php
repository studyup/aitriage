<?php
// getCurrentLocation is used only in this file so far.
if (!function_exists('getCurrentLocation')) {
	function getCurrentLocation($withLocal = false) {
		$path = Request::path();
		Log::debug('current location: '. $path);
		if (!$withLocal) {
			$currLocale = '/'.App::getLocale();
			$currLocaleLen = strlen($currLocale);
			if (substr($path, 0 - $currLocaleLen) == $currLocale) {
				$path = substr($path, 0, strlen($path) - $currLocaleLen);
			}
		}
		return $path;
	}
}
?>
<footer class="footer account-footer">
    <div class="container-full">
    	<div class="row" style="margin-right: auto;margin-left: auto">
    		<div class="col-sm-8">{{ trans('messages.site.footer') }}</div>
    		<div class="col-sm-4">
    			<ul class="list-inline pull-right">
    				<li class="{{ App::getLocale()=='en'    ? 'active':'' }}"><a href="/{{ getCurrentLocation() }}/en"   >English</a></li>
    				<li class="{{ App::getLocale()=='zh_hk' ? 'active':'' }}"><a href="/{{ getCurrentLocation() }}/zh_hk">Hong Kong 香港</a></li>
    				<li class="{{ App::getLocale()=='zh_cn' ? 'active':'' }}"><a href="/{{ getCurrentLocation() }}/zh_cn">中文简体</a></li>
    			</ul>
    		</div>
    	</div>
    </div>
</footer>

