; (function () {
    var install = function(Vue, options) {
        
        Vue.diagGetTermName = function(term) {
            if (term == undefined)
                return '';
        
            var returnName = '';
            if (typeof term === 'string')
                returnName = term;
            else if (term.display_name != undefined && term.display_name.length)
                returnName = term.display_name;
            else if (term.term_name != undefined && term.term_name.length)
                returnName =  term.term_name;
            else if (term.cc_display_name != undefined && term.cc_display_name.length)
                returnName = term.cc_display_name;
            else if (term.cc_term_name != undefined && term.cc_term_name.length)
                returnName = term.cc_term_name;
        
            if (returnName.length && Laravel.locale != 'zh_cn') {
                returnName = returnName.charAt(0).toUpperCase() + returnName.slice(1);
            }
            
            return returnName;
        },

        Vue.diagGetTermPair = function(parent, child) {
            if (child == undefined) {
                child = '';
            }
            if (parent == undefined) {
                parent = child;
                child = '';
            }
            if (parent == child) {
                child = '';
            }
            return parent.toString().trim() + '^' + child.toString().trim();
        },
        
        Vue.diagTrans = function(str) {
        	return (DiagLanguages.localizedStrings[str] == null)? str : DiagLanguages.localizedStrings[str];
        }
        
        Vue.isEmptyObject = function(e) {  
            var t;  
            for (t in e)  
                return !1;  
            return !0  
        },
        Vue.formatDate = function(dt) {
            if(dt == null) return '';
            var f = function(d) { return d < 10 ? '0' + d : d; };
            return dt.getFullYear() + '-' + f(dt.getMonth() + 1) + '-' + f(dt.getDate());
        }
    }
    
    if (typeof exports == 'object') {
        module.exports = install;

    } else if (typeof define == 'function' && define.amd) {
        define([], function () { return install });

    } else if (window.Vue) {
        Vue.use(install);
    }
})();