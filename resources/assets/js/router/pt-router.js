import Vue from 'vue'
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const chat = require('../components/chat/Chat.vue');

const router = new VueRouter({
    routes: [{
            path: '/',
            component: chat,
            meta: {
                keepAlive: true
            }
        },
    ],
    scrollBehavior(to, from, savedPosition) {
        if (to.path != '/')  {
            if (savedPosition) {
                return savedPosition
            } else {
                return {
                    x: 0,
                    y: to.meta.savedPosition || 0
                }
            }
        }
    }
});

export default router;