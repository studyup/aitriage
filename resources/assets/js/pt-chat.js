/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// add Mint UI for mobile
window.Mint = require('mint-ui');
Vue.use(Mint);

// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-default/index.css'
// Vue.use(ElementUI)

// add common function plugins
Vue.use(require('./lib/diag-helper'));

//lazy loading components
// __webpack_require__.p = '/build/';

const App = require('./components/chat/App.vue')


import store from './store/pt-store';
import router from './router/pt-router';
import * as types from './store/modules/patient/mutations_types'
$.ajaxSetup({
    error : function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

// console.log(store.state.token);

const whiteList = ['/login', '/register', '/reset', '/sendpwd']; // 不重定向白名单
router.beforeEach((to, from, next) => {
    next();
});

const app = new Vue({
    el: '#pt-chat',
    store,
    router,
    render: h => h(App)
})

window.app = app;