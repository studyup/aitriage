export const SET_CAN_PLAY_AUDIO = 'SET_CAN_PLAY_AUDIO';

export const SET_GENDER  = 'SET_GENDER';

export const SET_AGE  = 'SET_AGE';

export const SET_PREGNANCY = 'SET_PREGNANCY';
