import * as types from './mutations_types'
const mutations = {
    [types.SET_CAN_PLAY_AUDIO](state, data) {
        state.canPlayAudio = data;
    },
    [types.SET_GENDER](state, data) {
        state.Gender = data;
    },
    [types.SET_AGE](state, data) {
        console.log('set age='+data)
        state.age = data;
    },
    [types.SET_PREGNANCY](state, data) {
        state.age = data;
    },
}

export default mutations;