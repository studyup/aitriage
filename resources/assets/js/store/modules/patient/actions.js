import * as types from './mutations_types'

const actions = {
    TEXT2AUDIO({ commit, state }, data) {
        return new Promise((resolve, reject) => {
            $.get('/api/text2audio', { token: state.baiduToken, text: data }, function(res) {
                resolve(res);
            })
        });
    },
}

export default actions;