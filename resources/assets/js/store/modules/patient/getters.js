const getters = {
    isLogged: function(state) {
        return !!state.patientId 
    },
    partner: function() {
        return localStorage.getItem('EnvivePartner');
    },
    GenderLabel: function(state) {
        return state.Gender == '1' ? 'male' : 'female';
    }
}

export default getters;