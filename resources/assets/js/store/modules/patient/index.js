import state from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const store = {
    state: state,
    mutations: mutations,
    getters: getters,
    actions: actions
};
export default store;
