const state = {
    Gender: false,
    age: '',
    pregnancy: '',
    patientChatTheme: 'blue-theme',
    patientId: Envive.patientId,
    canPlayAudio: false,
    locale: Laravel.locale
}
export default state;