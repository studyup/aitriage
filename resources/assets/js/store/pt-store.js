import Vue from 'vue';
import patient from './modules/patient/index';

import Vuex from 'vuex';
// Vue.use(Vuex);
const store = new Vuex.Store(patient);

export default store
