<?php
if (env('FORCE_SCHEMA')) {
    URL::forceScheme(env('FORCE_SCHEMA'));
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
$default_locale = env('DEFAULT_LOCALE', 'zh_cn');

Route::get('/error/{errno}',function($errno){
    abort($errno);
});

Route::get('/', function () {
    return redirect('/patient/app');
});

Route::get('/', function () use($default_locale) {
    return redirect('/patient/app');
});

Route::get('/home/{locale}', function($locale) use($default_locale) {
    return redirect('/portal/'.$default_locale);
//	return view('home');
});

Route::get('/patient', function () {
    return redirect('/patient/zh_cn');
});

//portal index

Route::get ('/portal', function() use($default_locale) {
    return redirect('/portal/'.$default_locale);
});

Route::get('/patient/app',                            'PatientController@app');
Route::any('/verification',                           'PatientController@verification');//
// chat
Route::get ('/patient-chat/{locale}',                 'PatientController@chat');
//Route::group(['middleware'=>['auth.patient']], function (){
    Route::post('/patient/input-note/{locale}',         'PatientController@freeNoteParsing');
    Route::post('/patient/recommend-department',         'PatientController@recommendDepartment');
    Route::post('/patient/question',                    'PatientController@question');
//});
