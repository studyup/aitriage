const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.copy('node_modules/mint-ui/lib/style.min.css', 'public/css/mint-ui.min.css');
       
    mix.sass('pt-chat.scss')
		.webpack('pt-chat.js')
    	.version(['css/pt-chat.css']);
});
