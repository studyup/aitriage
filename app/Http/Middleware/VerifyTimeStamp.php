<?php

namespace App\Http\Middleware;

use App\Http\Helpers\EnvResponseHelper;
use Closure;
use Log;

class VerifyTimeStamp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $time = $request->input('time');
        if (!empty($time)){
            //verify timestamp
            $duration = abs(time()-$time);
            if ($duration <= 20){
                return $next($request);
            }
        }
        //reject the request and return code 300
        Log::info(sprintf('request: %s with content:%s reject by timestamp error',$request->url(), json_encode($request->all())));
        return EnvResponseHelper::sendResponse(EnvResponseHelper::ENVI_STATUS_TIMESTAMP_WRONG);
    }
}
