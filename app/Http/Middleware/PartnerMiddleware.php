<?php
/**
 * Created by PhpStorm.
 * User: kunzhang
 * Date: 2018/1/24
 * Time: 下午7:12
 */

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Route;
use App\Models\PartnerModelBase;
use App\Utils\DiagConstant;
use App\Http\Helpers\DiagHelper;
use App\Http\Helpers\EnvResponseHelper;
use Closure;
use App;
use BrowserDetect;
use Session;


class PartnerMiddleware
{

    /**
     * 在中间件中统一控制Locale
     * 添加手机端和桌面端浏览器的检测跳转
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Route::current()->hasParameter('locale') ?
            Route::current()->parameter('locale') : $request->input('locale');
        if (empty($locale))
            $locale = Session::get('locale');
        if (empty($locale))
            $locale = 'zh_cn';
        if (!empty($locale)) {
            Session::put('locale', $locale);
            App::setLocale($locale);
        }

        $partner = Route::current()->hasParameter('partner') ?
            Route::current()->parameter('partner') : $request->input('partner');
        if (empty($partner)) {
            $partner = Session::get(DiagConstant::SESSION_PARTNER_NAME_KEY);
        }
        if (empty($partner)) {
            $partner = DiagConstant::PARTNER_NAME_ENVIVE;
        }
        PartnerModelBase::setPartner($partner);
        Session::put(DiagConstant::SESSION_PARTNER_NAME_KEY, $partner);

        return $next($request);
    }
}