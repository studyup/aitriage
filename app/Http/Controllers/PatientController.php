<?php

namespace App\Http\Controllers;

use function GuzzleHttp\Psr7\build_query;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use GuzzleHttp\Client;
use App;
use BrowserDetect;
use KBAPI;
use function GuzzleHttp\json_decode;
use App\Http\Helpers\DiagHelper;
use App\Models\ActionType;
use App\Models\PartnerModelBase;
use App\Utils\DiagConstant;
use Cache;
use Validator;
use Input;
use Carbon\Carbon;

class PatientController extends Controller
{
    protected $patient;

    const SESSION_PATIENT_INFO_KEY     = 'patientInfo';
    const SESSION_USER_INPUT_FREE_NOTE = 'freeNote';

    const SESSION_PATIENT_CHAT_KEY = 'patient-chat';
    const CODE = 'verification';

    public function __construct() {
        $this->patient = new \App\Models\Patient();
    }

    public function app(Request $req) {
        return View::make('patient.chat')
            ->with('partner', DiagConstant::PARTNER_NAME_ENVIVE);
    }

    private function getBaiduToken() {
        $token = Cache::get('BAIDU_ACCESS_TOKEN');
        if (!$token) {
            $requestType = 'POST';
            $baiduUrl = 'https://openapi.baidu.com/oauth/2.0/token';
            $postBody = array();
            $postBody['grant_type'] = 'client_credentials';
            $postBody['client_id'] = 'Rv97RFnkWU7xdRgfeG8QIZ7s';
            $postBody['client_secret'] = 'lMZ84PI2d5XvgndBm9FvZjcfKKgKRjO7';
            $client = new \GuzzleHttp\Client();
            $res = $client->request($requestType, $baiduUrl.'?'.build_query($postBody), $postBody);
            $returnData = $res->getBody()->getContents();
            $returnData = json_decode($returnData, true);
            $token = $returnData['access_token'];

            $expiresAt = Carbon::now()->addDays(25);
            Cache::put('BAIDU_ACCESS_TOKEN', $token, $expiresAt);
        }
        return $token;
    }

    public function freeNoteParsing(Request $req, String $locale) {
        $freeNote = $req->input('note');
        $userIps = $req->getClientIps();
        $partner = $req->input('partner');
        $gender = $req->input('Gender');
        $age = $req->input('age');
        $patientInfo = array('age'=>$age, 'gender'=>$gender);
        $response = KBAPI::parsingText($freeNote, $patientInfo, $partner, $locale, $userIps);
        $response_arr = json_decode($response, true);
        $terms = array();
        $cc_hpis = array();
        foreach($response_arr as $term) {
            $cc_hpi = $term['cc_symid'].'^'.$term['symid'];
            if (!in_array($cc_hpi, $cc_hpis)) {
                $terms[] = $term;
                $cc_hpis[] = $cc_hpi;
            }
        }

        return response()->json($terms);
    }

    public function text2audio(Request $req) {
        $text = $req->input('text');
        $textMd5 = md5($text);
        $filePath = storage_path().DiagConstant::AUDIO_PATH . substr($textMd5, 0, 1);
        $file = '/'.$filePath. '/' .$textMd5.'.mp3';

        if (!file_exists($file)) {
            if (!is_dir($filePath)) {
                @mkdir($filePath, 0777, true);
            }
            $token = $this->getBaiduToken();
            $url = 'http://tsn.baidu.com/text2audio?tok=' . $token . '&lan=zh&ctp=1&cuid=78:4f:43:96:dc:36&tex='. urlencode($text) .'&per=0&spd=6';
            @file_put_contents($file, @file_get_contents($url));
        }

        if (is_file($file)) {
            header('Content-Length: ' . filesize($file), true);
            header("Content-Type: audio/mp3", true);
            readfile($file);
        } else {
            \Log::debug("load baidu audio file failed: " . $text);
        }
        exit;
    }

    public function question(Request $request, $keywords = null, $locale = null) {
        if (empty($keywords)) {
            $queryterms = $request->input('qt');
        } else {
            // convert to array
            $queryterms = explode(',', $keywords);
        }

        if (empty($locale)) {
            $locale = $request->input('locale');
        }

        if (is_array($queryterms)) {
            $queryterms = array_unique($queryterms);
        }
        else {
            $queryterms = array();
        }

        $gender = $request->input('Gender');
        $age = $request->input('age');
        $patientInfo = array('age'=>$age, 'gender'=>$gender);

        $symptoms_info_json = array();
        $userIps = $request->getClientIps();
        $partner = Session::get(DiagConstant::SESSION_PARTNER_NAME_KEY);
        $result = KBAPI::diagnosis(
                $queryterms, $patientInfo, $partner, $locale, $userIps, true);

        $data = json_decode($result, true);
        $queried_terms = $data['queried_terms'];
        $additional_symptoms = $data['additional_symptoms'];

        $res = array();
        $res['terms'] = $data['additional_symptoms'];
        $res['possible_diseases'] = $data['possible_diseases'];
        return json_encode($res);
    }

    public function recommendDepartment(Request $request, $keywords = null, $locale = null) {
        $dept = '';
        if (empty($keywords)) {
            $queryterms = $request->input('qt');
        } else {
            // convert to array
            $queryterms = explode(',', $keywords);
        }

        if (empty($locale)) {
            $locale = $request->input('locale');
        }

        if (is_array($queryterms)) {
            $queryterms = array_unique($queryterms);
        }
        else {
            $queryterms = array();
        }

        $gender = $request->input('Gender');
        $age = $request->input('age');
        $patientInfo = array('age'=>$age, 'gender'=>$gender);

        $symptoms_info_json = array();
        $userIps = $request->getClientIps();
        $partner = Session::get(DiagConstant::SESSION_PARTNER_NAME_KEY);
        $result = KBAPI::diagnosis(
                $queryterms, $patientInfo, $partner, $locale, $userIps, true);

        $data = json_decode($result, true);

        $possibleDiseases = $data['possible_diseases'];
        $diseaseName = '';
        if (empty($possibleDiseases))
            $possibleDiseases = array();
        if (count($possibleDiseases) > 0) {
            $diseaseName = $possibleDiseases[0]['display_name'];
        }

        $res =  App\Models\DepartmentDisease::where('disease', '=', $diseaseName)->distinct('distict')->select(['department_id'])->with('department')->get()->toArray();
        if ($res && count($res) > 0) {
            $dept = $res[0]['department']['name'];
        }
        return response()->json($dept);
    }
}
