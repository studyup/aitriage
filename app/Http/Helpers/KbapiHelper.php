<?php

namespace App\Http\Helpers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use function GuzzleHttp\json_decode;
//use Psy\Util\Json; // Better to use this json_encode() by adding default options.

use App\Http\Helpers\CacheHelper;
use App\Http\Helpers\DiagHelper;
use App\Http\Helpers\DiagPatientDataHelper;
use App\Models\Doctor;
use App\Models\Patient;
use App\Models\User;
use App\Models\FbCase;
use App\Utils\DiagConstant;
use Illuminate\Support\Facades\Auth;
use \Session;

class KbapiHelper
{
    const HTTP_REQUEST_TYPE_GET                 = 'GET';
    const HTTP_REQUEST_TYPE_POST                = 'POST';
    const HTTP_RESPONSE_CODE_OK                 = 200;
    const KBAPI_SEARCH_TERM_URL_PATTERN         = '%s/%s/symptomterm/%s/%s';
    const KBAPI_SEARCH_TERM_POST_URL_PATTERN    = '%s/%s/symptomterm3';
            const KBAPI_DIAGNOSIS_POST_URL_PATTERN      = '%s/%s/dx';
        const KBAPI_DIAGNOSIS_EMPTY_RESULT          = '{"queried_terms":[],"possible_diagnosis":[],"additional_symptoms":[],"pe_list":[]}';
        const KBAPI_PARSING_TEXT_PATTERN            = '%s/parsing_text/%s';
    const KBAPI_BODY_EMPTY_RESULT               = '[]';
    const KBAPI_SIGNATURE_SECRET_FORMAT         = 'Envi-%s-%d-%s-%s-%s-%d-Key'; // api-version, time, app-user-id, secret-per-caller, server-ip, body-length
    const KBAPI_JSON_REQUEST_HEADER             = array('Content-Type' => 'application/json',
                                                        'Accept'       => 'application/json');
    const CACHE_QUERY_BODY_EXCEPT_ITEMS         = array('patientInfo', 'doctorInfo', 'userIPs');
    const CACHE_QUERY_PATIENT_INCLUDE_ITEMS     = array('age', 'age_number', 'gender', 'risk_factors');

    private $appUserId;
    private $kbapiSecret; // secret per caller/userId
    private $kbapiUrlBase;
    private $kbapiUrlNlp;
    private $apiVersion;
    private $apiClient;
    private $apiCacheHelper;

    public function __construct() {
        $this->appUserId    = DiagHelper::getAppUserId();
        $this->kbapiSecret  = DiagHelper::getKbapiSecret();
        $this->kbapiUrlBase = env('KBAPI_BASE_URL', '');
        $this->kbapiUrlNlp  = env('KBAPI_NLP_URL', '');
        $this->apiVersion   = env('KBAPI_VERSION', 'v2');
        $this->apiClient    = new \GuzzleHttp\Client();
        $this->apiCacheHelper = new CacheHelper();
        if (empty($this->kbapiUrlBase)) {
            Log::warning('kbapiUrlBase is empty');
        }
        //Log::info("KBAPI base URL: " . $this->kbapiUrlBase . ", version: " . $this->apiVersion);
    }

    public function getApiVersion() {
        return $this->apiVersion;
    }

    private function generatePostHeaderBody(array $body) {
        $serverIP = array_key_exists('SERVER_NAME', $_SERVER) ? $_SERVER['SERVER_NAME'] : gethostname();
        $postTime = time();
        $body['postTime'] = $postTime;
        $body['serverIP'] = $serverIP;
        $body['userId']   = $this->appUserId;

        if (isset($body['doctorInfo'])) {
            unset ($body['doctorInfo']);
        }
        $body['doctorInfo'] = array();

        $bodyContent = \GuzzleHttp\json_encode($body);
        $bodyLength  = strlen($bodyContent);

        $secret = md5(
                sprintf(self::KBAPI_SIGNATURE_SECRET_FORMAT,
                        $this->apiVersion,
                        $postTime,
                        $this->appUserId,
                        $this->kbapiSecret,
                        $serverIP,
                        $bodyLength));
        $signature = md5(hash_hmac('sha256', $bodyContent, $secret));

        $headers = self::KBAPI_JSON_REQUEST_HEADER;
        $headers['signature'] = $signature;

        return array('headers' => $headers, 'body' => $bodyContent);
    }

    /*
     * Send request and log response time. Return the response object to caller.
     */
    private function sendKbapiRequest($requestType, $kbapiUrl, array $body) {
        $startRequestTime = microtime(true);

        if ($requestType == self::HTTP_REQUEST_TYPE_POST) {
            // try to read query result from cache first.
            $cacheQueryBody = array_except($body, self::CACHE_QUERY_BODY_EXCEPT_ITEMS);
            $cacheQueryBody['patientInfo'] = array_only($body['patientInfo'], self::CACHE_QUERY_PATIENT_INCLUDE_ITEMS);

            //sort the qt in $cacheQueryBody
            if (isset($cacheQueryBody['qt'])) {
                sort($cacheQueryBody['qt']);
                //Log::debug('-----cacheQueryBody[qt]: ' . var_export($cacheQueryBody['qt'], true));
            }

            //Log::debug('-----cacheQueryBody: ' . var_export($cacheQueryBody, true));
            $returnData = $this->apiCacheHelper->getApiQueryCache($requestType, $kbapiUrl, $cacheQueryBody);

            if (empty($returnData)) {
                // if not found in cache, read from kbapi.
                $postHeaderBody = $this->generatePostHeaderBody($body);
                Log::debug('kbapi query body: ' . $postHeaderBody['body']);
                $postHeaderBody['exceptions'] = false; // no exceptions for http_status != 200
                $res = $this->apiClient->request($requestType, $kbapiUrl, $postHeaderBody);
                $returnData = $res->getBody()->getContents();
                if ($res->getStatusCode() == self::HTTP_RESPONSE_CODE_OK) {
                    // save the kbapi query result to cache.
                    $this->apiCacheHelper->saveApiQueryCache($requestType, $kbapiUrl, $returnData, $cacheQueryBody);
                }
            }
        }
        else { // assume GET
            $returnData = $this->apiCacheHelper->getApiQueryCache($requestType, $kbapiUrl);
            if (empty($returnData)) {
                $res = $this->apiClient->request($requestType, $kbapiUrl);
                $returnData = $res->getBody()->getContents();
                if ($res->getStatusCode() == self::HTTP_RESPONSE_CODE_OK) {
                    $this->apiCacheHelper->saveApiQueryCache($requestType, $kbapiUrl, $returnData);
                }
            }
        }

        $completeRequestTime = microtime(true);
        $totalRequestTime = $completeRequestTime - $startRequestTime;
        Log::info(sprintf('kbapi res time: %01.3f sec, url: %s', $totalRequestTime, $kbapiUrl));

        return $returnData;
    }

    public function getPatientInfo($patientInfo) {
        $age = $patientInfo['age'];
        $gender = $patientInfo['gender'];
        $age_number = 1;
        if ($age == 1) {
            $age_number = 3;
        } else if ($age == 2) {
            $age_number = 9;
        } else if ($age == 3) {
            $age_number = 25;
        } else if ($age == 4) {
            $age_number = 48;
        } else if ($age == 5) {
            $age_number = 65;
        }
        $patientInfo['age_number'] = $age_number;

        Log::info(var_export($patientInfo, true));

        if ($patientInfo['age_number'] >= DiagPatientDataHelper::PATIENT_AGE_ADULT) {
            $patientInfo['age'] = 'adult';
        }
        else if ($patientInfo['age_number'] >= DiagPatientDataHelper::PATIENT_AGE_CHILD) {
            $patientInfo['age'] = 'child';
        }
        else if ($patientInfo['age_number'] >= DiagPatientDataHelper::PATIENT_AGE_INFANT) {
            $patientInfo['age'] = 'infant';
        }
        else {
            $patientInfo['age'] = 'newborn';
        }
        if (isset($patientInfo['gender'])) {
            if ('男' == $patientInfo['gender'] || '1' == $patientInfo['gender']) {
                $patientInfo['gender'] = 'male';
            } else if ('女' == $patientInfo['gender'] || '2' == $patientInfo['gender']) {
                $patientInfo['gender'] = 'female';
            }
        }

        Log::info('patientInfo = ' . \GuzzleHttp\json_encode($patientInfo));
        return $patientInfo;
    }

    public function diagnosis($queryterms, $patientInfo, $partner, $locale, $userIps, $queryFromPatient) {
        if (empty($this->kbapiUrlBase) || empty($queryterms) || empty($locale) || !is_array($queryterms)) {
            Log::debug('cannot send diagnosis request');
            return self::KBAPI_DIAGNOSIS_EMPTY_RESULT;
        }

        Log::info(sprintf('request diagnosis partner: %s', $partner));
        Log::info("request diagnosis queryterms: " . implode(',', $queryterms) . " locale: $locale ");

        $patientInfo = $this->getPatientInfo($patientInfo);
        $kbapiUrl = sprintf(self::KBAPI_DIAGNOSIS_POST_URL_PATTERN, $this->kbapiUrlBase, $this->apiVersion);
        $body = array(
                'qt' => $queryterms,
                'xt' => array(),
                'pe' => array(),
                'xpe' => array(),
                'vital' => array(),
                'locale' => $locale,
                'fromPatient' => 1,
                'patientInfo' => $patientInfo,
                'partner' => $partner,
                'userIPs' => $userIps // // end user IPs array
        );

        $res = $this->sendKbapiRequest(self::HTTP_REQUEST_TYPE_POST, $kbapiUrl, $body);

        $returnData = array();
        if (!empty($res)) {
            $returnData = json_decode($res, true);
        }

        if (empty($returnData) || empty($returnData['queried_terms'])) {
            $queryterms = array();
        }

        if ($queryFromPatient) {
            if (empty($returnData['possible_diseases'])) {
                $possibleDiseases = array();
            }
            else {
                $possibleDiseases = $returnData['possible_diseases'];
            }
            // also save possible diseases for doctor suggestion later
            //$this->apiCacheHelper->savePatientInput($patientInfo['id'], $queryterms, $possibleDiseases);
        }

        if (empty($returnData)) {
            return self::KBAPI_DIAGNOSIS_EMPTY_RESULT;
        }
        else {
            return \GuzzleHttp\json_encode($returnData);
        }
    }

    public function parsingText($freeNote, $patientInfo, $partner, $locale, $userIps) {
        if (empty($this->kbapiUrlBase) || empty($this->kbapiUrlNlp) || empty($locale)) {
            return self::KBAPI_BODY_EMPTY_RESULT;
        }

        $patientInfo = $this->getPatientInfo($patientInfo);
        Log::debug('xxx');
        Log::debug($patientInfo);
        $kbapiUrl = sprintf(self::KBAPI_PARSING_TEXT_PATTERN, $this->kbapiUrlNlp, $locale);
        $body = array(
                'text' => $freeNote,
                'locale' => $locale,
                'patientInfo' => $patientInfo,
                'partner' => $partner,
                'userIPs' => $userIps // an array
        );

        $res = $this->sendKbapiRequest(self::HTTP_REQUEST_TYPE_POST, $kbapiUrl, $body);

        if (!empty($res)) {
            return $res;
        }

        return self::KBAPI_BODY_EMPTY_RESULT;
    }

}
