<?php

namespace App\Http\Helpers;

use App\Models\Patient;
use App\Models\PatientVisit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_decode;
// use Psy\Util\Json; // Better to use this json_encode() by adding default options.
use App\Models\Locale;
use App\Models\PartnerModelBase;
use App;

class DiagPatientDataHelper
{
	const NOTE_TYPE = 1;

	// all comparison is >= this age_number
	const PATIENT_AGE_INFANT  = 0.1;
	const PATIENT_AGE_CHILD   = 1;
	const PATIENT_AGE_ADULT   = 17;
	const PATIENT_AGE_ELDERLY = 65;

	public function __construct()
	{
		parent::__construct();
	}

}
