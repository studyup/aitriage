<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Log;

use DB;
use Illuminate\Http\Request;
use function GuzzleHttp\json_decode;
use App;

class DiagHelper
{
	const APP_USER_ID_ENV    = 'APP_USER_ID';   // developer name or server name. used as kbapi caller.
	const KBAPI_SECRET_ENV   = 'KBAPI_SECRET';  // secret per caller/developer/production server
	const CACHE_ENABLED_ENV  = 'CACHE_ENABLED'; // TRUE|true string will be convert to TRUE, FALSE|false will be convert to FALSE.
	const DEFAULT_LOCALE_ENV = 'DEFAULT_LOCALE'; // name in .env
	const DEFAULT_LOCALE_VAL = 'zh_cn'; // consider initial release in China
	const API_SIGNATURE_SECRET_FORMAT = '%s-%s-%s-%d-%s';
	const HTTP_METHOD_GET 	 = 'GET';
	const HTTP_METHOD_POST   = 'POST';

	private static $appUserId;
	private static $kbapiSecret;
	private static $cacheEnabled = NULL;
	
	private static $locale;
	private static $escapedLocale;

	public static function SignatureLog($message, $context = array()){
	    return;
	    return Log::debug($message, $context);
    }
	/**
	 * Based on env.
	 * @return: developer id, server id, or empty string if the env config not found.
	 */
	public static function getAppUserId() {
		if (empty(self::$appUserId)) {
			self::$appUserId = trim(env(self::APP_USER_ID_ENV, ''));
		}
		return self::$appUserId;
	}
	
	/**
	 * Based on env. A secret per caller.
	 * @return: secret as string.
	 */
	public static function getKbapiSecret() {
		if (empty(self::$kbapiSecret)) {
			self::$kbapiSecret = trim(env(self::KBAPI_SECRET_ENV, ''));
		}
		return self::$kbapiSecret;
	}
	
	/**
	 * Based on env.
	 * @return: true or false
	 */
	public static function isCacheEnabled() {
		if (self::$cacheEnabled === NULL) {
			self::$cacheEnabled = env(self::CACHE_ENABLED_ENV, TRUE);
		}
		return self::$cacheEnabled;
	}
	
	/**
	 * Based on env default locale.
	 * @return: locale name like zh_cn.
	 */
	public static function getDefaultLocale() {
		if (empty(self::$locale)) {
			self::$locale = env(self::DEFAULT_LOCALE_ENV, self::DEFAULT_LOCALE_VAL);
		}
		return self::$locale;
	}

	/**
	 * Based on env default locale.
	 * @return: escaped locale qith single quote like 'zh_cn'.
	 */
	public static function getEscapedDefaultLocale() {
		if (empty(self::$escapedLocale)) {
			self::$escapedLocale = DB::connection()->getPdo()->quote(self::getDefaultLocale());
		}
		return self::$escapedLocale;
	}
	
	/**
	 * Based on given locale.
	 * @return: escaped locale qith single quote like 'zh_cn'. if $locale is empty, return escaped default locale.
	 */
	public static function getEscapedLocale($locale) {
		if (empty($locale)) {
			return self::getEscapedDefaultLocale();
		}
		return DB::connection()->getPdo()->quote($locale);
	}
	
	/**
	 * 返回IP 地址
	 * @return [type] [description]
	 */
	public static function getRealIp()
	{
		//self::SignatureLog(var_export($_SERVER, true));
	    $ip=false;
	    if(!empty($_SERVER["HTTP_CLIENT_IP"])){
	        $ip = $_SERVER["HTTP_CLIENT_IP"];
	    }
	    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	        $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
	        if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
	        for ($i = 0; $i < count($ips); $i++) {
	            if (!preg_match("/^(10│172.16│192.168)./i", $ips[$i])) {
	                $ip = $ips[$i];
	                break;
	            }
	        }
	    }
	    return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
	}
	
	/**
	 * backdoor请求的clientIP是否在白名单
	 * return bool
	 */
	public static function isInWhiteIPlistForBackdoor() {
		$whiteIPs = array(
			"127.0.0.1",
			"116.228.88.188", 	// henghui Road 88 Lane
            "::1"
		);
		return in_array(DiagHelper::getRealIp(), $whiteIPs);
	}

	/**
	 * 是否可以跳过验证
	 * return bool
	 */
	public static function isEnableToSkipVerifySignature() {
		if (Request::capture()->input('backdoor')  === 'envi@2016y' or App::environment('local')) {
//			self::SignatureLog('skip verifySignature');
			return true;
		}
		return false;
	}

	public static function startsWith($haystack, $needle)
	{
		return strncmp($haystack, $needle, strlen($needle)) === 0;
	}
	
	public static function endsWith($haystack, $needle)
	{
		return $needle === '' || substr_compare($haystack, $needle, -strlen($needle)) === 0;
	}
}
