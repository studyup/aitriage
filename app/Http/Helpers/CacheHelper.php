<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

use App\Http\Helpers\DiagHelper;

class CacheHelper
{
	const PATIENT_INPUT_CACHE_TIME 		 	= 5;  // minutes
	const PATIENT_INPUT_CACHE_KEY_FORMAT	= 'Patient-Input-Cache-%d';

	const KBAPI_QUERY_CACHE_TIME       	 	= 30; // minutes
	const KBAPI_QUERY_CACHE_KEY_RAW_FROMAT 	= '%s-%s-%s'; // request-type, api-url, request-body-string
	const KBAPI_QUERY_CACHE_KEY_FORMAT     	= 'KBAPI-Cache-Key-%s'; // md5 hash of KEY_RAW string
	
	private function getPatientInputCacheKey($patientId) {
		return sprintf(self::PATIENT_INPUT_CACHE_KEY_FORMAT, $patientId);
	}
	
	public function getPatientInput($patientId) {
		if (empty($patientId)) {
			return FALSE;
		}

		$patientKey = $this->getPatientInputCacheKey($patientId);
		$patientData = Cache::get($patientKey);
		if (empty($patientData)) {
			return FALSE;
		}

		Log::debug("Get cached patient input: " . $patientData);
		return unserialize($patientData);
	}

	public function savePatientInput($patientId, $queryterms, $possibleDiseases) {
		if (empty($patientId) || !is_array($queryterms)) {
			return FALSE;
		}
	
		$patientKey = $this->getPatientInputCacheKey($patientId);
		$patientData = array(
				'qt' => $queryterms,
				'possible_diseases' => $possibleDiseases,
				'time' => time(),
		);
		
		$patientDataToCache = serialize($patientData);
		Cache::put($patientKey, $patientDataToCache, self::PATIENT_INPUT_CACHE_TIME);
		Log::debug("Saved patient input to cache: " . $patientDataToCache);
		return TRUE;
	}

	private function getApiQueryCacheKey($requestType, $kbapiUrl, $requestBody) {
		$requestContent = json_encode($requestBody);
		$keyHash = md5(sprintf(self::KBAPI_QUERY_CACHE_KEY_RAW_FROMAT,
				$requestType, $kbapiUrl, $requestContent));
		return sprintf(self::KBAPI_QUERY_CACHE_KEY_FORMAT, $keyHash);
	}
	
	public function getApiQueryCache($requestType, $kbapiUrl, $requestBody = false) {
		if (!DiagHelper::isCacheEnabled()) {
			return FALSE;
		}
		
		if (empty($requestType) or empty($kbapiUrl)) {
			return FALSE;
		}
		 
		$apiCacheKey = $this->getApiQueryCacheKey($requestType, $kbapiUrl, $requestBody);
		$serializedData = Cache::get($apiCacheKey);
		if (empty($serializedData)) {
			return FALSE;
		}
	
		Log::debug("Get cached API result: ". $apiCacheKey); // . '=' . $serializedData);
		return unserialize($serializedData);
	}
	
	public function saveApiQueryCache($requestType, $kbapiUrl, $data, $requestBody = false) {
		if (!DiagHelper::isCacheEnabled()) {
			return FALSE;
		}
		
		if (empty($requestType) or empty($kbapiUrl)) {
			return FALSE;
		}
	
		$apiCacheKey = $this->getApiQueryCacheKey($requestType, $kbapiUrl, $requestBody);
		$serializedData = serialize($data);
		Cache::put($apiCacheKey, $serializedData, self::KBAPI_QUERY_CACHE_TIME);
	
		Log::debug("Saved API result to cache: ".$apiCacheKey); //. '=' . $serializedData);
		return TRUE;
	}
	
}
