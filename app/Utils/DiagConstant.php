<?php

namespace App\Utils;

class DiagConstant
{
	// DB related
	const PARTNER_DB_ID_PREFIX     = 'envi_web_';
	const PARTNER_NAME_ENVIVE      = 'envive'; // internal/demo
	// Session related
	const SESSION_PATIENT_INFO_KEY = 'patientInfo';
	const SESSION_DOCTOR_ID_KEY    = 'doctorId';
	const SESSION_PARTNER_NAME_KEY = 'partnerName';

	const AUDIO_PATH = '/audios/';
}
