<?php

namespace App\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

    }


    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map(Request $req)
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        // 自动加载路由
        $routes = $this->app['router']->getRoutes()->get($req->getMethod());
        $hasOne = Arr::first($routes, function ($value) use ($req) {
            return $value->matches($req, true);
        });
        if(!$hasOne) {
            $this->mapAutoRoutes($req);
        }
    }

    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    protected function mapAutoRoutes(Request $req)
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) use ($req) {
            $uriArr = $req->segments();
            $controller = $action = $argsStr = '';
            $k = 1;
            for ($i = 0; $i < count($uriArr); $i++) {
                if($i == 0) {
                    $controller = $uriArr[$i];
                    continue;
                }

                if($i == 1) {
                    $action = $uriArr[$i];
                    continue;
                }
                $argsStr .= '{arg' . $k . '?}/';
                $k++;
            }
            // if have args must have action
            // args: arg1 arg2 ...
            // if no action call controller's index action
            $newUri = '/' . $controller . '/'  . ($action ? $action . '/' : '') . $argsStr;
            $newUri = rtrim($newUri, '/');
            $action = $action ? $action : 'index';
            Route::match(['get', 'post'], $newUri, ucfirst($controller) . 'Controller@' . $action);
            // Run controller check exist
            try {
                $currentController = $this->app->make('App\Http\Controllers\\' . ucfirst($controller) . "Controller");
                if(!method_exists($currentController, $action)) {
                    throw new NotFoundHttpException;
                }
            } catch(\Exception $ex) {
                abort(404);
            }
        });
    }


}
