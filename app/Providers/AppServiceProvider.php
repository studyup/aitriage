<?php

namespace App\Providers;

use Illuminate\Routing\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	DB::listen(function ($query) {
    	    $sql = str_replace("?", "'%s'", $query->sql);
    	    $log = vsprintf($sql, $query->bindings);
    		Log::debug($log);
    	});
        view()->composer("layouts.app", function($view) {
            $scriptFile = '';
            $uriStr = $this->app->make("router")->current()->uri();
            $uriArr = explode('/',trim($uriStr, '/'));
            $controller = array_shift($uriArr);
            $action = array_shift($uriArr);

            if($controller) {
                $scriptPath = public_path(trim(env('JS_LOCATION', '/js/module'), '/') . '/');
                $scriptFile = $scriptPath . $controller . '.js';
                if($action) {
                    if (!preg_match("/{.+}/i", $action)) {
                        //  controller_action.js
                        $scriptFile = $scriptPath . $controller . '_' . $action . '.js';
                    }
                }
            }
            $data = [];
            $data['controller'] = $controller;
            $data['action'] = $action;
            if(file_exists($scriptFile)) {
                $data['script'] = '<script type="text/javascript" src="' . substr($scriptFile,strlen(public_path())) . '"></script>';
            }
            $view->with('currentData',$data);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
