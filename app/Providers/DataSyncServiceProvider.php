<?php 

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
* Data Sync Service Provider
*/
class DataSyncServiceProvider extends ServiceProvider
{
	/**
     * Bootstrap any application services.
     *
     * @return void
     */
	public function boot()
	{
	}

	/**
     * Register any application services.
     *
     * @return void
     */
	public function register()
	{

	}
}