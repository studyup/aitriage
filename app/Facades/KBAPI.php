<?php
/**
 * Created by PhpStorm.
 * User: Magi
 * Date: 17/4/18
 * Time: 13:56
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class KBAPI extends  Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Http\Helpers\KbapiHelper';
    }

}