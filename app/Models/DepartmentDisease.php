<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentDisease extends PartnerModelBase
{
    protected $table = 'department_disease';

    public function department() {
    	return $this->belongsTo('\App\Models\Department', 'department_id', 'id');
    }
}
