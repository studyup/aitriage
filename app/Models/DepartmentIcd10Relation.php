<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentIcd10Relation extends Model
{
    //
    protected $table = 'envi_web_common.department_icd10_relations';


//    protected $fillable = [];
    // protected $appends = ['medication'];
    protected $hidden = ['created_at', 'updated_at'];


    public static function getDepartmentNamesByIcd10Names($icd10_names) {
        $results = DepartmentIcd10Relation::whereIn('icd10_name', $icd10_names)->distinct('distict')->select(['department'])->get()->toArray();

        $resultStr = '';
        foreach ($results as $rel) {
            $resultStr .= $rel['department'].',';
        }

        $resultStr = rtrim($resultStr, ',');
        return $resultStr;
    }

}
