<?php

namespace App\Models;


class PatientVisit extends PartnerModelBase
{
    protected $table = 'patient_visit';

    protected $fillable = [
        'pat_id', 'pat_locale_id', 'doctor_id', 'doc_locale_id', 'visit_type'
    ];

}
