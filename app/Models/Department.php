<?php

namespace App\Models;

class Department extends PartnerModelBase
{
    protected $table = 'department';
    protected $fillable = ['name'];

}
