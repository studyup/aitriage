<?php

namespace App\Models;

class Hospital extends PartnerModelBase
{
    protected $table = 'hospital';

    protected $fillable = ['hname'];
}