<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\DiagConstant;
use Log;
use Session;

/**
 * Base class for all models/tables in customer database.
 * Shared models/tables should derive from Model directly and use ..._common database.
 */
class PartnerModelBase extends Model
{
	/**
	 * The partner name.
	 *
	 * @var string
	 */
	private static $partner;

	/**
	 * The partner names and associated DB identifiers.
	 *
	 * @var array
	 */
    private static $supportedPartners = array(
        'fromfuture' => 'cn_shanghai_xuhui',    // 徐汇云医院
        'fromfuture_lyg' => 'cn_lyg',           // 贯众 - 连云港
        'wuhan'      => 'cn_wuhan',
        'sxyygh'     => 'cn_sxyygh',
        'baiyang'    => 'cn_baiyang',           // baiyang
    );

	/**
	 * The database associated with the derived partner model.
	 *
	 * @var string
	 */
	private static $partnerDbIdentifier;


	private static $backupDispatcher;

	public $timestamps = false;

	/**
	 * Overwrite:
	 * Get the table associated with the model.
	 *
	 * @return string
	 */
	public function getTable()
	{
		$table = parent::getTable();
		if (empty(self::$partnerDbIdentifier)) {
			Log::error(__METHOD__ . ' Missing partner DB identifier for table ' . $this->table);
		}
		else {
			$table = self::$partnerDbIdentifier . '.' . $table;
		}
		return $table;
	}

	/**
	 * Get the table name with db identifier associated with the model.
	 *
	 * @return string
	 */
	public static function getTableWithIdentifier()
	{
		$derivedModel = get_called_class();
		$obj = new $derivedModel();
		return $obj->getTable();
	}

	public static function setPartner($partnerName) {

		if ($partnerName === null) {
			$partnerName = '';
		}
		self::$partner = $partnerName;
		self::setPartnerDbIdentifierByName($partnerName);
	}

	public static function getPartnerDbIdentifier() {
		return self::$partnerDbIdentifier;
	}

	public static function setPartnerDbIdentifier($dbIdentifier) {
		self::$partnerDbIdentifier = $dbIdentifier;
	}

	public static function setDispatcherStatus($status)
	{
		if ($status){
			self::setEventDispatcher(self::$backupDispatcher);
		}else{
			self::$backupDispatcher = self::getEventDispatcher();
			self::unsetEventDispatcher();
		}
	}

	/**
	 * Set the db identifier by partner name.
	 *
	 * @param $partnerName string the partner name.
	 */
	private static function setPartnerDbIdentifierByName($partnerName) {
		$partnerDbId = DiagConstant::PARTNER_NAME_ENVIVE;
		$partnerName = mb_strtolower($partnerName);
		if (isset(self::$supportedPartners[$partnerName])) {
			$partnerDbId = self::$supportedPartners[$partnerName];
		}
		self::setPartnerDbIdentifier(DiagConstant::PARTNER_DB_ID_PREFIX . $partnerDbId);
    }
}
