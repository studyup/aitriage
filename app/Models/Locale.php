<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
	protected $table = 'locale';
	
	protected $fillable = [
		'name'
	];
	
	public $timestamps = false;
	private static $locales = array(
		'en' => 1,
		'zh_hk' => 2,
		'zh_cn' => 3
	);
	
    public static function getLocaleIdByName($name) {
    	if (array_key_exists($name, self::$locales)) {
    		return self::$locales[$name];
    	} else {
    		$locale = self::firstOrCreate(['name' => $name]);
    		self::$locales[$name] = $locale->id;
    		return $locale->id;
    	}
    }
}
