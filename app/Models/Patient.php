<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use App\Http\Helpers\Crypt;

class Patient extends PartnerModelBase implements
AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;
    protected $table = 'patients';

    protected $fillable = [
        'external_id', 'name', 'gender', 'day_of_birth', 'password', 'mobile','email', 'idcard', 'is_smoker', 'is_pregnant', 'hospital_code', 'hospital_id', 'pmhx_selected', 'medhx_selected', 'riskfactor_selected', 'rxalergy_selected', 'cvd_risk_score'
    ];
    
    protected $hidden = array('password');

    protected $appends = ['age', 'age_number'];

    private $locale = '';

    /**
     * over ride : setter & getter
     */
    public function getAttribute($key) {

        if (in_array($key, [/*'name','day_of_birth','gender'*/])) {
            return Crypt::encrypt(parent::getAttribute($key),'D','envive');
        }
        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, [/*'name','day_of_birth','gender'*/])) {
            $value = Crypt::encrypt($value,'E','envive');
        }
        return parent::setAttribute($key, $value);
    }

    /**
     * override function Model::save()
     * Save the model to the database.
     *
     * @param  array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if (strlen($this->day_of_birth) == 0) {
            $this->day_of_birth = null;
        }
        if (empty($this->gender)) {
            $this->gender = 0;
        }
        return (parent::save($options));
    }

    public function getGenderAttribute($value)
    {
        $locale = empty($this->locale) ? App::getLocale() : $this->locale;
        return Gender::getGender($locale, $value);
    }

    public static function caculateAge($birthday) {
        $age = 0;
        if (empty($birthday)) 
            return trans('patient.AgeNone');

        $diff = \Carbon\Carbon::parse($birthday)->diff(\Carbon\Carbon::now());

        if ($diff->y <= 2) {
            if ($diff->m > 0) {
                return array('number'=> (12 * $diff->y + $diff->m), 'diff_y'=>$diff->y, 'diff_m'=> $diff->m, 'diff_d'=> $diff->d, 'unit_label'=> trans('patient.Months'), 'unit'=>'m');
            } else {
                return array('number'=>$diff->d, 'diff_y'=>$diff->y, 'diff_m'=> $diff->m, 'diff_d'=> $diff->d, 'unit_label'=> trans('patient.Days'), 'unit'=>'d');
            }
        } else {
            return array('number'=>$diff->y, 'diff_y'=>$diff->y, 'diff_m'=> $diff->m, 'diff_d'=> $diff->d, 'unit_label'=>trans('patient.Years'), 'unit'=>'y');
        }
    }
    public function getAgeAttribute()
    {
        $response =  self::caculateAge($this->day_of_birth);
        if (is_array($response))
            return $response['number'].$response['unit_label'];
        else
            return $response;
    }

    public function getAgeNumberAttribute() {
        if(empty($this->day_of_birth)) return 0;
        $response =  self::caculateAge($this->day_of_birth);
        if (array_key_exists('diff_y', $response))
            return number_format($response['diff_y'] + $response['diff_m'] / 12  + $response['diff_d'] / 365, 3);
        else
            return 0;
    }

    public function findByExternalId($external_id)
    {
        return $this->where(['external_id' => $external_id])->first();
    }
}