<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Symptoms extends Model
{
    //
    protected $table = 'symptoms';

    public static function getSymsByIds(Array $ids)
    {
        $res = self::whereIn('id', $ids)->get(['id as symid','name as display_name'])->toArray();

        return $res;
    }
}
