<?php

namespace App\Models;


class Role
{
    private static $roles = array(
        '0' => 'common',
        '1' => 'feedback',
        '2' => 'review',
        '3' => 'demo',
        '4' => 'master',
    );

    public static function getRole($id){
        if (array_key_exists($id, self::$roles)) {
            return self::$roles[$id];
        } else {
            return 'admin';
        }
    }
}