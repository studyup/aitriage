<?php

namespace App\Models;


class Gender
{
    private static $genders = array(
        'en' => array(
            '0' => 'Unknown',
            '1' => 'Male',
            '2' => 'Female'
        ),
        'zh_hk' =>array(
            '0' => '未知',
            '1' => '男',
            '2' => '女'
        ),
        'zh_cn' => array(
            '0' => '未知',
            '1' => '男',
            '2' => '女'
        )
    );

    public static function getGender($locale, $id){
        if (array_key_exists($locale, self::$genders)) {
            if (array_key_exists($id, self::$genders[$locale])) {
                return self::$genders[$locale][$id];
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
}