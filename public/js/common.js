function enviGetDisplaynameWithoutMasterName(currName, masterName) {
	// Handle Chinese displaynames
	if (Laravel.locale == 'zh_cn') {
		return currName.replace(masterName, '');
	}
	
	// Handle English displaynames
	var finalName = "";
	var currNameLower = currName.toLowerCase();
	var masternameLower = masterName.toLowerCase();
	if (currNameLower.startsWith(masternameLower + ' ')) {
		finalName = currName.substr(masterName.length + 1);
	}
	else if (currNameLower.endsWith(' ' + masternameLower)) {
		finalName = currName.substr(0, currName.length - masterName.length - 1);
	}
	else {
		var insideIndex = currNameLower.indexOf(' ' + masternameLower + ' ');
		if (insideIndex > 0) {
			// Example: master: Abdominal pain, detailed: Lower abdominal pain worse during menstruation
			// Should return 'Lower abdominal pain worse during menstruation'.
			finalName = currName.substr(0, insideIndex) +
				        currName.substr(insideIndex + masternameLower.length + 1);
		}
		else {
			finalName = currName;
		}
	}
    return finalName.charAt(0).toUpperCase() + finalName.slice(1);
}

function enviGetTermName(term) {
	if (term == undefined)
		return '';

	var returnName = '';
	if (typeof term === 'string')
		returnName = term;
	else if (term.display_name != undefined && term.display_name.length)
		returnName = term.display_name;
	else if (term.term_name != undefined && term.term_name.length)
		returnName =  term.term_name;
	else if (term.cc_display_name != undefined && term.cc_display_name.length)
		returnName = term.cc_display_name;
	else if (term.cc_term_name != undefined && term.cc_term_name.length)
		returnName = term.cc_term_name;

	if (returnName.length && Laravel.locale != 'zh_cn') {
		returnName = returnName.charAt(0).toUpperCase() + returnName.slice(1);
	}
	
	return returnName;
}