const { mix } = require('laravel-mix');
const webpack = require('webpack')
const path = require('path')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath(path.normalize('public/build'));
// console.log(path.normalize('public/' + build))

mix.options({
    processCssUrls: true
});
// mix.webpackConfig({
//     output: {
//          chunkFilename: 'js/[name].[chunkhash].js',// : 'js/[name].js',
//          filename: '[name].js',
//          path: path.resolve(__dirname, 'build')
//         //  publicPath: mix.inProduction() ? '/build/' : '/'
//     }
// })

mix.extract([
        'jquery',
        'bootstrap-sass',
        'vue',
        'vuex',
    ], 'js/vendor.js')
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
        vue: ['Vue', 'window.Vue'],
        vuex: ['Vuex']
    })

mix.copy('node_modules/mint-ui/lib/style.min.css', 'public/build/css/mint-ui.min.css');

mix.js('resources/assets/js/pt-chat.js', 'js').sourceMaps()
    .sass('resources/assets/sass/pt-chat.scss', 'css')

mix.copyDirectory(
    'node_modules/bootstrap-sass/assets/fonts/bootstrap',
    'public/fonts/vendor/bootstrap-sass/bootstrap/'
);

mix.copyDirectory(
    'node_modules/element-ui/lib/theme-chalk/fonts',
    'public/fonts/vendor/element-ui/lib/theme-chalk'
);

// mix.copyDirectory(
//     'node_modules/font-awesome/fonts',
//     'public/fonts/vendor/font-awesome'
// );
if (mix.inProduction()) {
    mix.version();
}
mix.disableNotifications();